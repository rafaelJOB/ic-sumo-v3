import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;

public class Matching {
	
	List<Mapping> listMap;
	String outFile;
	
	public Matching(String _file) {
		listMap = null;
		outFile = _file;
	}
	
	void compare(DomainOntology onto1, UpperOntology onto2) throws FileNotFoundException, IOException {
		List<Concept> listC1 = onto1.get_listConcepts();
		List<Mapping> listM = new ArrayList<Mapping>();
		for(Concept cnp1: listC1) {
			Mapping map = new Mapping();
			System.out.println(cnp1.get_className());
			
			cnp1.print_info();
			if(cnp1.get_goodSynset() != null) {
				String code = "" + cnp1.get_goodSynset().getOffset();
				code = code_fixation(code);
				System.out.println("CODE:" + code);
				
				try(BufferedReader br = new BufferedReader(new FileReader("C:/Users/PLN-2017/Desktop/BackUp-Rafael/Rafael/WordNetMappings30-noun.txt"))) {
					StringBuilder sb = new StringBuilder();
					String line = "FIRST READ";			// = br.readLine();
		    	
					while (line != null) {
						sb.append(line);
						sb.append(System.lineSeparator());
						line = br.readLine();

						if(line.startsWith(code)) {
							String aux = null;
							Concept cnp2 = null;
							//System.out.println(line);
							aux = recover_ali(line);
							System.out.println(aux);
							
							map.set_source(cnp1.get_owlClass().getIRI().toString());	//*
							cnp2 = recover_upperConcept(aux,onto2);

							if(cnp2.get_owlClass() == null) {
								map.set_target("null");
							} else {
								map.set_target(cnp2.get_owlClass().getIRI().toString());
								cnp1.set_aliClass(cnp2.get_owlClass());
							}
							
							if(aux.endsWith("=")) {
								map.set_relation("=");
							} else if(aux.endsWith("+")) {
								map.set_relation("&gt;");
							}
							map.set_measure("1.0");
							listM.add(map);
							break;
						}
					}
					br.close();
				}
					
			} else {
			
			map.set_source(cnp1.get_classID());
			map.set_target("null");
			map.set_relation("null");
			map.set_measure("false");
			listM.add(map);
			}
			System.out.println("----------");
		}
		listMap = listM;
		out_rdf(onto1, onto2);
	}
	
	Concept recover_upperConcept(String line, UpperOntology onto2) {
		List<Concept> listC2 = onto2.get_listConcepts();
		Concept cnp = new Concept();
		for(Concept cnp2: listC2) {
			if(line.endsWith("=")) {
				line = line.replace("=", "");
			} else if(line.endsWith("+")) {
				line = line.replace("+", "");
			}
			
			if(cnp2.get_className().equals(line)) {
				cnp = cnp2;
			}
		}
		return cnp;
	}
		
	
	String code_fixation(String code) {
		
		if(code.length() == 7) {
			code = "0" + code;
		} else if(code.length() == 6) {
			code = "00" + code;
		} else if(code.length() == 5) {
			code = "000" + code;
		} else if(code.length() == 4) {
			code = "0000" + code;
		} else if(code.length() == 3) {
			code = "00000" + code;
		} else if(code.length() == 2) {
			code = "000000" + code;
		} else if(code.length() == 1) {
			code = "0000000" + code;
		}
		return code;
	}
	
	String recover_ali(String line) {
		
		String aux;
		int i = line.indexOf("&%");
		
		aux = line.substring(i);
		aux = aux.replaceFirst("&%", "");
		return aux;
	}
	
	void out_rdf(DomainOntology onto1, UpperOntology onto2 ) throws IOException {
		FileWriter arq = new FileWriter(outFile);
		PrintWriter print = new PrintWriter(arq);
		
		print.print("<?xml version='1.0' encoding='utf-8' standalone='no'?>\n" + 
						"<rdf:RDF xmlns='http://knowledgeweb.semanticweb.org/heterogeneity/alignment#'\n" +
						"\t\t xmlns:rdf='http://www.w3.org/1999/02/22-rdf-syntax-ns#'\n" +
						"\t\t xmlns:xsd='http://www.w3.org/2001/XMLSchema#'\n" + 
						"\t\t xmlns:align='http://knowledgeweb.semanticweb.org/heterogeneity/alignment#'>\n");
		
		print.print("<Alignment>\n" + 
						"\t<xml>yes</xml>\n" + 
						"\t<level>0</level>\n" + 
						"\t<type>11</type>\n");
		
		print.print("\t<onto1>\n" + "\t\t<Ontology rdf:about=" + '"' + onto2.get_ontologyID().getOntologyIRI().toString() + '"' + ">\n" + 
						"\t\t\t<location>file:" + onto2.get_fileName() + "</location>\n" + 
							"\t\t</Ontology>\n" + "\t</onto1>\n");
		
		print.print("\t<onto2>\n" + "\t\t<Ontology rdf:about=" + '"' + onto1.get_ontologyID().getOntologyIRI().toString() + '"' + ">\n" + 
				"\t\t\t<location>file:" + onto1.get_fileName() + "</location>\n" + 
					"\t\t</Ontology>\n" + "\t</onto2>\n");
		
		for(Mapping m: listMap) {
			if(!m.get_measure().equals("false")) {
				print.print(toRDF(m));				//adc if
			}
		}
		
		print.print("</Alignment>\n" + "</rdf:RDF>");
		
		arq.close();
		System.out.println("Arquivo .rdf gerado!");
	}
	
String toRDF(Mapping m) {
		
		String out = "\t<map>\n" +
				"\t\t<Cell>\n" +
				"\t\t\t<entity1 rdf:resource=\""+ m.get_target() +"\"/>\n" +
				"\t\t\t<entity2 rdf:resource=\""+ m.get_source() +"\"/>\n" +
				"\t\t\t<measure rdf:datatype=\"http://www.w3.org/2001/XMLSchema#float\">"+ m.get_measure() +"</measure>\n" +
				"\t\t\t<relation>" + m.get_relation().toString() + "</relation>\n" + "\t\t</Cell>\n" + "\t</map>\n";
		return out;		
	}
}
