import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import edu.mit.jwi.Dictionary;
import edu.mit.jwi.IDictionary;

public class BaseSynset {

	private String wnhome = "C:/Users/PLN-2017/Desktop/BackUp-Rafael/Rafael/ONTOBRAS-matching/WordNet-3.0"; //Local do WordNet
	private IDictionary dict;
	
	private List<String> stopWords;
	private List<String> adjWords;
	
	public BaseSynset() {
		dict = null;
		stopWords = new ArrayList<String>();
		adjWords = new ArrayList<String>();
	}
	
	protected IDictionary get_dictionary() {
		return dict;
	}
	
	protected List<String> get_stpWords() {
		return stopWords;
	}
	
	protected List<String> get_adjWords() {
		return adjWords;
	}
	
	protected void init() {
		rc_dictionary();
		rd_StpWords();
		rd_AdjWords();
	}
	
	private void rc_dictionary() {
		try {
			String path = wnhome + File.separator + "dict" ;
			URL url = new URL("file",null,path) ;
			dict = new Dictionary(url);
		} catch(MalformedURLException e) {
			System.out.println("URL do WN mal formada!");
			System.out.println("erro: " + e);
		}
	}
	
	private void rd_StpWords() {
		try {
			BufferedReader br = new BufferedReader(new FileReader("C:/Users/PLN-2017/Desktop/BackUp-Rafael/Rafael/stopwords2.txt"));
			String line;
			
			while((line = br.readLine()) != null) {
				stopWords.add(line.toLowerCase());
			}
			br.close();
			
		} catch(FileNotFoundException e) {
	    	System.out.println("Arquivo das StopWords n�o encontrado!");
	    	System.out.println("erro: " + e);
	    } catch (IOException e) {
	    	System.out.println("Opera��o I/O interrompida, no arquivo das StopWords!");
	    	System.out.println("erro: " + e);
	    }

	}
	
	private void rd_AdjWords() {
		try {
			BufferedReader br = new BufferedReader(new FileReader("C:/Users/PLN-2017/Desktop/BackUp-Rafael/Rafael/adjetivos.txt"));
			String line;
			
			while((line = br.readLine()) != null) {
				adjWords.add(line);
			}
			br.close();
			
		} catch(FileNotFoundException e) {
	    	System.out.println("Arquivo das AdjWords n�o encontrado!");
	    	System.out.println("erro: " + e);
	    } catch (IOException e) {
	    	System.out.println("Opera��o I/O interrompida, no arquivo das AdjWords!");
	    	System.out.println("erro: " + e);
	    }
	}
	
}
