
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.TreeMap;

import org.semanticweb.owlapi.apibinding.OWLManager;
import org.semanticweb.owlapi.model.IRI;
import org.semanticweb.owlapi.model.OWLClass;
import org.semanticweb.owlapi.model.OWLClassExpression;
import org.semanticweb.owlapi.model.OWLOntology;
import org.semanticweb.owlapi.model.OWLOntologyCreationException;
import org.semanticweb.owlapi.model.OWLOntologyID;
import org.semanticweb.owlapi.model.OWLOntologyManager;
import org.semanticweb.owlapi.model.OWLOntologyStorageException;

public class Ontology {
	
	private String fileName;
	
	protected OWLOntologyID ontologyID;
	protected OWLOntology ontology;
	protected OWLOntologyManager manager;
	
	protected List<Concept> listConcept;
	
	
	public Ontology() {
		listConcept = new ArrayList<Concept>();
	}
	
	private void set_fileName(String _file) {
		int i = _file.lastIndexOf("/");
		String fname;
		
		fname = _file.substring(i);
		
		fileName = fname;
	}
	
	protected String get_fileName() {
		return fileName;
	}
	
	protected void set_ontologyID(OWLOntologyID ontoID) {
		ontologyID = ontoID;
	}
	
	protected OWLOntologyID get_ontologyID() {
		return ontologyID;
	}
	
	protected void set_ontology(OWLOntology onto) {
		ontology = onto;
	}
	
	protected OWLOntology get_ontology() {
		return ontology;
	}
	
	protected void set_ontologyManager(OWLOntologyManager _manager) {
		manager = _manager;
	}
	
	protected OWLOntologyManager get_ontologyManager() {
		return manager;
	}
	
	protected List<Concept> get_listConcepts() {
		return listConcept;
	}
	
	protected void ShouldLoad(String _file) throws OWLOntologyCreationException, OWLOntologyStorageException, FileNotFoundException, IOException {
		
		File file = new File(_file);
		
		set_fileName(_file);
		
		OWLOntologyManager manager = OWLManager.createOWLOntologyManager();
		
		OWLOntology local = manager.loadOntologyFromOntologyDocument(file);
		IRI documentoIRI = manager.getOntologyDocumentIRI(local);
		
		set_ontologyManager(manager);
		set_ontology(local);
		
		System.out.println("Loaded Ontology: " + local);
		System.out.println("From: " + documentoIRI + "\n");
		
		//manager.saveOntology(local);
		
		OWLOntologyID ID = local.getOntologyID();
		set_ontologyID(ID);
		
		System.out.println(ID);
		System.out.println(ID.getOntologyIRI() + "\n");		
	}
	
	protected void extract_superClass(OWLClass cls, HashSet<String> set, TreeMap<String, Integer> map, List<OWLClassExpression> list) {
		
		int cont = 0;
		for(OWLClassExpression sup: cls.getSuperClasses(ontology)) {
        	
        	if(!sup.isAnonymous()) {					
        		//System.out.println("SuperClasseID: " + sup);
        		//System.out.println("SuperClasse: " + sup.asOWLClass().getIRI().getFragment() + "\n");
        		
        		cont++;
        		set.add(sup.asOWLClass().getIRI().getFragment().toString());
        		map.put(sup.asOWLClass().getIRI().getFragment(), cont);
        		list.add(sup);
        		
        		//extract_supAnnotation(sup, set);
        		extract_superRecurClass(sup, set, map, cont, list);	
                	
        	}	
		}
		
	}
	
	
	protected void extract_superRecurClass(OWLClassExpression su, HashSet<String> set, TreeMap<String, Integer> map, int cont, List<OWLClassExpression> list) {	
		if(su != null) {
			cont++;
			for(OWLClassExpression sup: su.asOWLClass().getSuperClasses(ontology)) {
        	
				if(!sup.isAnonymous()) {					
					//System.out.println("SuperClasseID: " + sup);
					//System.out.println("SuperClasse: " + sup.asOWLClass().getIRI().getFragment() + "\n");
					list.add(sup);
					set.add(sup.asOWLClass().getIRI().getFragment().toString());
					map.put(sup.asOWLClass().getIRI().getFragment(), cont);
					
					//extract_supAnnotation(sup, set);
					extract_superRecurClass(sup, set, map, cont, list);
        			
                	
				}	
			}
		}		
	}
	
	protected void extract_subClass(OWLClass cls, HashSet<String> set, TreeMap<String, Integer> map, List<OWLClassExpression> list) {
		
		int cont = 0;
		cont--;
		for(OWLClassExpression sub: cls.getSubClasses(ontology)) {

        	if(!sub.isAnonymous()) {
        		//System.out.println("SubClasseID: " + sub);
        		//System.out.println("SubClasse: " + sub.asOWLClass().getIRI().getFragment() + "\n");
        		
        		set.add(sub.asOWLClass().getIRI().getFragment().toString());
        		map.put(sub.asOWLClass().getIRI().getFragment(), cont);
        		list.add(sub);
        		
        		//extract_subAnnotation(sub, set);
        		extract_subRecurClass(sub, set, map, cont, list);
        		
        	}
        }
	}
	
	protected void extract_subRecurClass(OWLClassExpression su, HashSet<String> set, TreeMap<String, Integer> map, int cont, List<OWLClassExpression> list) {
		//int aux = cont;
		//aux = aux - 1;
		if(su != null) {
			cont--;
			for(OWLClassExpression sub: su.asOWLClass().getSubClasses(ontology)) {
				
				if(!sub.isAnonymous()) {
					//System.out.println("SubClasseID: " + sub);
	        		//System.out.println("SubClasse: " + sub.asOWLClass().getIRI().getFragment() + "\n");
	        		
	        		list.add(sub);
	        		set.add(sub.asOWLClass().getIRI().getFragment().toString());
	        		map.put(sub.asOWLClass().getIRI().getFragment(), cont);
	        		
	        		//extract_subAnnotation(sub, set);
	        		extract_subRecurClass(sub, set,  map, cont, list);
				}
			}
		}
	}
		
}
