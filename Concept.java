import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;

import org.semanticweb.owlapi.model.OWLClass;
import org.semanticweb.owlapi.model.OWLClassExpression;

import edu.mit.jwi.item.ISynset;

public class Concept {
	
	
	private OWLClass owlClass;
	private String ontologyID;
	private String classID;
	
	private String ontologyName;
	private String className;
	
	private Map<String, Integer> distance;				//Relaciona o nome da classe com sua distacia da classe "principal"
	private String desc;
	private Set<String> context;					//Cria um SET(n�o permite itens repitidos) para o contexto utiliza o HASH para melhor performance de adi��o de itens
	
	private List<String>stpWords;
	
	private List<OWLClassExpression> supers;
	private List<OWLClassExpression> subs;
	
	private ISynset goodSynset;
	
	private Map<ISynset, List<String>> synsetCntxt;
	
	private OWLClass aliClass;
	
	private int numSy;
	
	public Concept() {
		distance = new TreeMap<String, Integer>();
		context = new HashSet<String>();
		synsetCntxt = new LinkedHashMap<ISynset, List<String>>();
		stpWords = new ArrayList<String>();
		numSy = -1;
	}
	
	void set_owlClass(OWLClass owlclass) {
		owlClass = owlclass; 
	}
	
	OWLClass get_owlClass() {
		return owlClass;
	}
	
	void set_ontologyID(String _ontologyID) {
		ontologyID = _ontologyID;
	}
	
	String get_ontologyID() {
		return ontologyID;
	}

	void set_classID(String _classID) {
		classID = _classID;
	}
	
	String get_classID() {
		return classID;
	}
	
	void set_ontologyName(String _ontologyName) {
		ontologyName = _ontologyName;
	}
	
	String get_ontologyName() {
		return ontologyName;
	}
	
	void set_className(String _className) {
		className = _className;
	}
	
	String get_className() {
		return className;
	}
	
	void set_distance(TreeMap<String, Integer> map) {
		distance = map;
	}
	
	Map<String, Integer> get_distance() {
		return distance;
	}
	
	void set_desc(String _desc) {
		desc = _desc; 
	}
	
	String get_desc() {
		return desc;
	}
	
	void set_context(Set<String> set) {
		context = set;
	}
	
	Set<String> get_context() {
		return context;
	}
	
	void set_stpWords(List<String> list) {
		stpWords = list;
	}
	
	List<String> get_stpWords() {
		return stpWords;
	}
	
	void set_supers(List<OWLClassExpression> _supers) {
		supers = _supers;
	}
	
	List<OWLClassExpression> get_supers() {
		return supers;
	}
	
	void set_subs(List<OWLClassExpression> _subs) {
		subs = _subs;
	}
	
	List<OWLClassExpression> get_subs() {
		return subs;
	}
	
	void set_goodSynset(ISynset _goodSynset) {
		goodSynset = _goodSynset;
	}
	
	ISynset get_goodSynset() {
		return goodSynset;
	}
	
	void set_synsetCntx(HashMap<ISynset, List<String>> _synsetCntxt) {
		synsetCntxt = _synsetCntxt;
	}
	
	Map<ISynset, List<String>> get_synsetCntx() {
		return synsetCntxt;
	}
	
	void set_aliClass(OWLClass _aliclass) {
		aliClass = _aliclass; 
	}
	
	OWLClass get_aliClass() {
		return aliClass;
	}
	
	void set_numSy(int num) {
		numSy = num;
	}
	
	int get_numSy() {
		return numSy;
	}
	
	public void print_info() {
		
		System.out.println("Concept: " + this.className);
		System.out.println("Description: " + this.desc + "\n");
		if(this.goodSynset != null) {
			System.out.println("Synset: " + this.goodSynset.toString());
			System.out.println("Gloss: " + this.goodSynset.getGloss().toString() + "\n");
		} else {
			System.out.println("Synset: null");
		}

		String out = "Contexto: ";
		Iterator<String> iterator = this.context.iterator();
		while(iterator.hasNext()) {
			String a = iterator.next();
			if(!iterator.hasNext()) {
				out = out + a + ".";
			} else {
				out = out + a + ", "; 
			}
		}
		System.out.println(out + "\n");
	}
	
	protected void init() {
		Set<String> aux = sp_string(this.context);
		Set<String> temp = new HashSet<String>();
		for(String word: aux) {
			if(hasUpperCase(word)) {					//add teste para words sepradas por uppercase que estavam dentro da descri��o
				String tempStr = rm_specialChar(word);
				temp.addAll(sp_upperCase(tempStr));
			} else {
				String tempStr = rm_specialChar(word);
				temp.add(tempStr.toLowerCase());
			}
		}
		temp = rm_stopWords(temp);
		set_context(temp);
	}
	
	private Set<String> sp_string(Set<String> context) {
		Set<String> temp= new HashSet<String>();
		
		for(String str: context) {
			String[] split = null;
			
			if(str.contains("_")) {
				split = str.split("_");
				
				for(String strSplit: split) {
					temp.add(strSplit.toLowerCase());
				}
				
			} else if(hasWhiteSpace(str)) {
				
				split = str.split(" |�");
				
				for(String strSplit: split) {
					temp.add(strSplit);			//rm .tolower
				}
				
			} else if(hasUpperCase(str)) {
				temp.addAll(sp_upperCase(str));
			} else {
				temp.add(str);
			}	
		}
		context.clear();
		context = temp;
		
		return context;
	}
	
	private boolean hasWhiteSpace(String str) {
		
		int length = str.length();
		
		for(int y = 1; y < length; y++) {
			if(Character.isWhitespace(str.charAt(y))) {
				return true;
			}	
		}
		return false;	
	}
	
	private Set<String> rm_stopWords(Set<String> set) {
		
	    Set<String> wordSet = new HashSet<String>();
	    List<String> stpWords = get_stpWords();
		for(String word: set) {            
			String wordLow = word.toLowerCase();
			
	        if(!(stpWords.contains(wordLow))) {
	                
	        	if( !(word.equals(" ") || word.equals("-") || word.equals("")) ) {
	                	wordSet.add(wordLow);
	            } 
	        }  
	    }
		return wordSet;
	}
	
	private Set<String> sp_upperCase(String wordComp) {
		Set<String> sep = new HashSet<String>();
		int x = wordComp.length();
		int up, aux = 0;
		
		for(int y = 1; y < x; y++) {
			if(Character.isUpperCase(wordComp.charAt(y))) {
				up = y;
				sep.add(wordComp.substring(aux, up).toLowerCase());
				aux = up;
			}	
		}
		sep.add(wordComp.substring(aux).toLowerCase());
		return sep;
	}
	
	protected String sp_conceptName() {
		
		String name = null;
		String cnpName = this.className;
		
		if(cnpName.contains("_")) {
			
			String words[];
			words = cnpName.split("_");
			int i = words.length;
			name = words[i - 1];	
			
		} else if(cnpName.contains("-")) {
			
			String words[];
			words = cnpName.split("-");
			int i = words.length;
			name = words[i - 1];
			
		} else if(hasUpperCase(cnpName)) {
			
			int x = cnpName.length();
			int up = 0;
			for(int y = 1; y < x; y++) {
				if(Character.isUpperCase(cnpName.charAt(y)) && y > up) {
					up = y;	
				}	
			}
			if(up != 0) {
				name = cnpName.substring(up);
			}
			
		} else {
			
			name = cnpName;	
		}
		
		return name;
	}	
	
	
	String rm_specialChar(String word) {
		if(word.endsWith("-")) {
			word = word.replace("-", "");
		}
		
		if(word.contains("(")) {
        	word = word.replace("(", "");
        }
        
        if(word.contains(")")) {
        	word = word.replace(")", "");
        }
        
        if(word.contains(",")) {
        	word = word.replace(",", "");
        }
        
        if(word.contains(":")) {
        	word = word.replace(":", "");
        }        
        
        if(word.contains("'")) {
        	word = word.replace("'", "");
        }
        
        if(word.contains("?")) {
        	word = word.replace("?", "");
        }
        
        if(word.contains("!")) {
        	word = word.replace("!", "");
        }
        
        if(word.contains(".")) {
        	word = word.replace(".", "");
        }
        
        if(word.contains(";")) {
        	word = word.replace(";", "");
        }
        
        return word;
	}
	
	List<String> transfer_context() {
		List<String> list = new ArrayList<String>();
		
		for(String ctxt: this.context) {
			list.add(ctxt);
		}
		return list;
	}
	
	private boolean hasUpperCase(String word) {
		
		int x = word.length();
		
		for(int y = 1; y < x; y++) {
			if(Character.isUpperCase(word.charAt(y))) {
				return true;
			}	
		}
		return false;	
	}
	
}
